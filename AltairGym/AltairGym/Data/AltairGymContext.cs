﻿using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System;
using System.Threading.Tasks;
using System.Linq;
using AltairGym.Models;
using Microsoft.Extensions.Configuration;

namespace AltairGym.Data
{
    public partial class AltairGymContext:DbContext
    {
        public AltairGymContext(DbContextOptions<AltairGymContext>options)
        {
        }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            IConfiguration configuration = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json")
                .Build();

            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer(configuration.GetConnectionString("DefaultConnection"));
            }
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("Relational:Collation", "Modern_Spanish_CI_AS");
            modelBuilder.Entity<Lesson>().ToTable("LESSON");
            modelBuilder.Entity<Activities>().ToTable("ACTIVITIES");
            modelBuilder.Entity<Customer>().ToTable("CUSTOMER");
            modelBuilder.Entity<Room>().ToTable("ROOM");
            modelBuilder.Entity<Lesson>().ToTable("LESSON");
            modelBuilder.Entity<Trainer>().ToTable("TRAINER");
            OnModelCreatingPartial(modelBuilder);
        }
        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
        public DbSet<Activities> Activities { get; set; }
        public DbSet<Booking> Booking { get; set; }
        public DbSet<Lesson> Lesson { get; set; }
        public DbSet<Customer> Customer { get; set; }
        public DbSet<Trainer> Trainer { get; set; }
        public DbSet<Room> Room { get; set; }

    }
}
