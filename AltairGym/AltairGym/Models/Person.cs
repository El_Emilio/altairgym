﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace AltairGym.Models
{
    public abstract class Person
    {
        public int Id { get; set; }
        [Required]
        [StringLength(15, ErrorMessage ="Maximo 15 caracteres")]
        public string Name { get; set; }
        [Required]
        [StringLength(30, ErrorMessage = "Maximo 30 caracteres")]

        public string Surname { get; set; }
        [Required]
        [EmailAddress]
        public string Email { get; set; }
        [Required]
        [Phone]
        public string Phone { get; set; }
        [Required]
        public string DNI { get; set; }

    }
}
