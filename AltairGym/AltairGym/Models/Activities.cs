﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace AltairGym.Models
{
    public class Activities
    {
        public int Id { get; set; }
        [Required]
        public string Title { get; set; }
        [Required]
        public string Description { get; set; }
        public int Assistance { get; set; }
        public int Duration { get; set; }
        public Boolean Active { get; set; }

        public int TrainerId { get; set; }
        [ForeignKey("TrainerId")]
        public Trainer Trainer { get; set; }

        [InverseProperty("Activities")]
        public List<Lesson> Lessons { get; set; }

    }
}
