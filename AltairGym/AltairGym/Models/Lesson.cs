﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace AltairGym.Models
{
    public class Lesson
    {
        public int Id { get; set; }
        [Required]
        [DataType(DataType.Date)]
        
        public DateTime DateClass { get; set; }
        [Required]
        [DataType(DataType.Time)]

        public DateTime Hour { get; set; }
        [Required]
        public string Name { get; set; }

        public int RoomId { get; set; }
        [ForeignKey("RoomId")]
        public Room Room { get; set; }

        public int ActivitiesId { get; set; }
        [ForeignKey("ActivitiesId")]
        public Activities Activities { get; set; }

        [InverseProperty("Lesson")]
        public List<Booking> Bookings { get; set; }
    }
}
