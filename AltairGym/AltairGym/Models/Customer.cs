﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace AltairGym.Models
{
    public class Customer:Person
    {
        [Required]
        public float Weight { get; set; }

        [Required]
        public float Height { get; set; }
        
        public int Member { get; set;}
        [InverseProperty("Customer")]
        public List<Booking> Bookings { get; set; }
    }
}
