﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace AltairGym.Models
{
    public class Trainer:Person
    {
        [InverseProperty("Trainer")]
        public List<Activities> Activities { get; set; }
    }
}
