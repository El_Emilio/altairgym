﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using AltairGym.Data;
using AltairGym.Models;

namespace AltairGym.Controllers
{
    public class LessonsController : Controller
    {
        private readonly AltairGymContext _context;

        public LessonsController(AltairGymContext context)
        {
            _context = context;
        }

        // GET: Lessons
        public async Task<IActionResult> Index()
        {
            var altairGymContext = _context.Lesson.Include(l => l.Activities).Include(l => l.Room).Include(l=> l.Bookings);
            return View(await altairGymContext.ToListAsync());
        }

        public async Task<IActionResult> VistaReal()
        {
            var altairGymContext = _context.Lesson.Include(l => l.Activities).Include(l => l.Room).Include(l => l.Bookings).Where(l => l.DateClass >= DateTime.Now && l.Activities.Assistance <= l.Room.Capacity);
            return View(await altairGymContext.ToListAsync());
        }
        // GET: Lessons/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var lesson = await _context.Lesson
                .Include(l => l.Activities)
                .Include(l => l.Room)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (lesson == null)
            {
                return NotFound();
            }

            return View(lesson);
        }

        // GET: Lessons/Create
        public IActionResult Create()
        {
            ViewData["ActivitiesId"] = new SelectList(_context.Activities, "Id", "Description");
            ViewData["RoomId"] = new SelectList(_context.Room, "Id", "Name");
            return View();
        }

        // POST: Lessons/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,DateClass,Hour,Name,RoomId,ActivitiesId")] Lesson lesson)
        {
            if (ModelState.IsValid)
            {
                var correct = _context.Activities.FirstOrDefault(a => a.Id == lesson.ActivitiesId);
                if (!correct.Active)
                {
                    //No comprendo porque no me muestra el error cuando lo estas creando 
                    TempData["errorMessage"] = "ERROR this activity is inactive";
                    return RedirectToAction(nameof(Create));
                }
                _context.Add(lesson);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["ActivitiesId"] = new SelectList(_context.Activities, "Id", "Description", lesson.ActivitiesId);
            ViewData["RoomId"] = new SelectList(_context.Room, "Id", "Name", lesson.RoomId);
            return View(lesson);
        }

        // GET: Lessons/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var lesson = await _context.Lesson.FindAsync(id);
            if (lesson == null)
            {
                return NotFound();
            }
            ViewData["ActivitiesId"] = new SelectList(_context.Activities, "Id", "Description", lesson.ActivitiesId);
            ViewData["RoomId"] = new SelectList(_context.Room, "Id", "Name", lesson.RoomId);
            return View(lesson);
        }

        // POST: Lessons/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,DateClass,Hour,Name,RoomId,ActivitiesId")] Lesson lesson)
        {
            if (id != lesson.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(lesson);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!LessonExists(lesson.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["ActivitiesId"] = new SelectList(_context.Activities, "Id", "Description", lesson.ActivitiesId);
            ViewData["RoomId"] = new SelectList(_context.Room, "Id", "Name", lesson.RoomId);
            return View(lesson);
        }

        // GET: Lessons/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var lesson = await _context.Lesson
                .Include(l => l.Activities)
                .Include(l => l.Room)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (lesson == null)
            {
                return NotFound();
            }

            return View(lesson);
        }

        // POST: Lessons/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var lesson = await _context.Lesson.FindAsync(id);
            _context.Lesson.Remove(lesson);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool LessonExists(int id)
        {
            return _context.Lesson.Any(e => e.Id == id);
        }
    }
}
